CREATE DATABASE fastcontact;

USE fastcontact;

CREATE TABLE contacts (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    fullname VARCHAR(100),
    phone VARCHAR(20),
    email VARCHAR(100)
);