from flask import Flask, render_template, request, flash, url_for, redirect
from flask_mysqldb import MySQL

app:Flask = Flask(__name__)

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'fastcontact'

app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT/'

mySQL:MySQL = MySQL(app)

@app.route('/')
@app.route('/<id>')
def index( id:int = -1 ):
    cursor = mySQL.connection.cursor()
    cursor.execute('SELECT * FROM contacts')
    mySQL.connection.commit()
    contacts = cursor.fetchall()
    
    if id == -1:
        fullname = ''
        phone = ''
        email = ''
    else:
        try:
            cursor.execute(f'SELECT * FROM contacts WHERE id = {id}')
            mySQL.connection.commit()
            contact = cursor.fetchone()
            fullname = contact[1]
            phone = contact[2]
            email = contact[3]
        except:
            flash('El contacto no existe en la base de datos...')
            fullname = ''
            phone = ''
            email = ''
    
    context = {
        'contacts':contacts,
        'id':id,
        'fullname':fullname,
        'phone':phone,
        'email':email
    }
    return render_template('index.html', **context)

@app.route('/add_contact', methods=['GET','POST'])
def store():
    if request.method == 'POST':
        fullname = request.form['fullname']
        phone = request.form['phone']
        email = request.form['email']

        cursor = mySQL.connection.cursor()
        cursor.execute('INSERT INTO contacts (fullname,phone,email) VALUES (%s,%s,%s)',(fullname.lower(),phone,email.lower()))

        mySQL.connection.commit()
        flash('Registro creado con exito...')
    else:
        flash('No puede ingresar al link...')
    return redirect( url_for('index') )

@app.route('/edit_contact/<id>', methods=['GET', 'POST'])
def update( id:int ):
    if request.method == 'POST':
        fullname = request.form['fullname']
        phone = request.form['phone']
        email = request.form['email']
        cursor = mySQL.connection.cursor()
        cursor.execute('UPDATE contacts SET fullname = %s , phone = %s , email = %s WHERE id = %s',( fullname.lower(),phone,email.lower(),id ))

        mySQL.connection.commit()
        flash('Registro actualizado con exito...')
    else:
        flash('No puede ingresar al link...')
    return redirect( url_for('index') )


@app.route('/delete_contact')
@app.route('/delete_contact/<id>')
def delete( id = -1):
    if id != -1:
        cursor = mySQL.connection.cursor()
        cursor.execute(f'DELETE FROM contacts WHERE id = {id}')
        mySQL.connection.commit()
        flash('El contacto se elimino con exito...')
    else:
        flash('No puede ingresar al link...')
    return redirect( url_for('index') )

@app.errorhandler(404)
def error404(error):
    return render_template('error404.html', error=error)

if __name__ == '__main__':
    app.run( debug=True , port=9000 )