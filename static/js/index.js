window.addEventListener('load', () => {

    let btnHome = document.querySelector('#btn-home');

    let btnDelete = document.querySelectorAll('#btn-delete');

    if (btnHome != null) {
        btnHome.addEventListener('click', () => {
            window.location.href = 'http://127.0.0.1:9000'
        });
    }
    if (btnDelete != []) {
        btnDelete.forEach(self => {
            self.addEventListener('click', (event) => {

                event.preventDefault();

                Swal.fire({
                    title: 'Quiere eliminar el contacto?',
                    showDenyButton: true,
                    confirmButtonText: `Si...`,
                    denyButtonText: `No...`,
                    customClass: {
                        confirmButton: 'order-1',
                        denyButton: 'order-2',
                    }
                }).then((result) => {
                    if (result.isDenied) {
                        Swal.fire('No se borro el contacto', '', 'info');
                    }
                    else {
                        window.location.href = event.toElement.href;
                    }
                })

            })
        });
    }


});
